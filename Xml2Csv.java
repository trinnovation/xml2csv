import java.io.File;  
import javax.xml.parsers.DocumentBuilder;  
import javax.xml.parsers.DocumentBuilderFactory;  
import javax.xml.parsers.ParserConfigurationException;  
import javax.xml.transform.Transformer;  
import javax.xml.transform.TransformerException;  
import javax.xml.transform.TransformerFactory;  
import javax.xml.transform.dom.DOMSource;  
import javax.xml.transform.stream.StreamResult;  
  
import org.w3c.dom.Attr;  
import org.w3c.dom.Document;  
import org.w3c.dom.Element;  

public class Xml2Csv 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		System.out.println("**************************************************");
		System.out.println("Dit programma zet een XML file om in een CSV file.");
		System.out.println("Geschreven door: Giovanni Marjenburgh");
		System.out.println("**************************************************");

		// Creeer een .xml bestand
		
	//	CreateXML createXml = new CreateXML();
	//	createXml.createXML();
		ReadXML myReadXML = new ReadXML();
		myReadXML.readXML();
		
	}
}
