import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.nio.file.*;
import java.io.*;
import java.util.ArrayList;

public class ReadXML {
 public void readXML() {
  try {

   File xmlFile = new File("shopfeed.xml");
   FileOutputStream myOutputStream = new FileOutputStream("shopfeed.csv",false);
   PrintStream p = new PrintStream(myOutputStream);
  
   FileOutputStream myOutputStreamCat = new FileOutputStream("category.csv",false);
   PrintStream pcat = new PrintStream(myOutputStreamCat);
   
   DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
   DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
   Document doc = documentBuilder.parse(xmlFile);

   // Doc is het daadwerkelijke XML document
   // Nodelist is een specifieke lijst met nodes binnen het XML document
   // Node is de specifieke node uit de nodelijst
   
   doc.getDocumentElement().normalize();
   NodeList nodeList = doc.getElementsByTagName("item");

   System.out.println("Root element :"
     + doc.getDocumentElement().getNodeName());

   System.out.println("Totaal aantal producten: " + nodeList.getLength() + "\n");
   
   ArrayList<String> hoofdcat = new ArrayList<String>();//Hoofdcategory
   ArrayList<String> cat = new ArrayList<String>();//Category
   ArrayList<String> subcat = new ArrayList<String>();//Subcategory

  

   // ************************************************
   // Print alle hoofdcategorieen niet genaamd catalog
   // ************************************************
   
   p.println("ID~Title~Category~Price~Description~Image_Link~Quantity");
  for (int i=0;i<nodeList.getLength();i++)
   //for (int i=0;i<50;i++)
   {
	   Node myNode = nodeList.item(i);
	   //System.out.println(myNode.getNodeName());
	   Element myTitle = (Element) myNode;
	   String temp = new String();
	   //Char temp2 = new Char();
	   String[] strparts = myTitle.getElementsByTagName("link").item(0).getTextContent().split("/");
	
	   if (! strparts[3].equals("catalog"))
	   {
	  
		   p.print(myTitle.getElementsByTagName("g:id").item(0).getTextContent() + "~");
		   p.print(myTitle.getElementsByTagName("title").item(0).getTextContent() + "~");

		   p.print(strparts[strparts.length-3] + "," + strparts[strparts.length-2] + "~");
	   
		   temp = myTitle.getElementsByTagName("g:price").item(0).getTextContent();
		   temp = temp.replace("�", "");// Haal Euro teken weg
		   temp = temp.replace("\u00A0", "");// Haal white space weg
		   p.print(temp+"~");
		   
		   temp = myTitle.getElementsByTagName("description").item(0).getTextContent(); // Haal line breaks weg
		   
		   temp = temp.replace("\n", " ");
		   
		      
		   p.print(temp+"~");
		   //p.print(myTitle.getElementsByTagName("description").item(0).getTextContent() + "~");
		   p.print(myTitle.getElementsByTagName("g:image_link").item(0).getTextContent() + "~");
		   p.println(myTitle.getElementsByTagName("g:quantity").item(0).getTextContent());
	   }
	   
	  
		   if (! strparts[3].equals("catalog"))
	   {
		   if (cat.lastIndexOf(strparts[3])==-1)
		   {
		   cat.add(strparts[3]);
		   //System.out.println("home;"+strparts[3]);
		   pcat.println("home;"+strparts[3]);
		   
		   }
	   }
	   
   }
  
  
  // ************************************************
  // Print alle categorieen niet genaamd catalog
  // ************************************************
  
  
 
  for (int i=0;i<nodeList.getLength();i++)
  {
  
	   Node myNode2 = nodeList.item(i);
	   Element myTitle2 = (Element) myNode2;
	   String[] strparts = myTitle2.getElementsByTagName("link").item(0).getTextContent().split("/");
   
	   if (! strparts[3].equals("catalog"))
	   {
		   if (cat.lastIndexOf(strparts[4])==-1)
		   {
		   cat.add(strparts[4]);
		   //System.out.println(hoofdcat.lastIndexOf(strparts[3]));
		   //System.out.println(strparts[3]+"~"+strparts[4]);
		   pcat.println(strparts[3]+"~"+strparts[4]); //Schrijf naar file

		   }
	   }
   }
  
  // ************************************************
  // Print alle subcategorieen niet genaamd catalog
  // ************************************************
  
  
 
  
  for (int i=0;i<nodeList.getLength();i++)
  {
  
	   Node myNode3 = nodeList.item(i);
	   Element myTitle3 = (Element) myNode3;
	   String[] strparts = myTitle3.getElementsByTagName("link").item(0).getTextContent().split("/");
   
	   if (! strparts[3].equals("catalog") && (strparts.length > 6)) // Zorg ervoor de laatste gedeelte van url wordt genegeerd
	   {
		   if (subcat.lastIndexOf(strparts[5])==-1)
		   {
		   subcat.add(strparts[5]);
		  // System.out.println(strparts[4]+"~"+strparts[5]);
		   pcat.println(strparts[4]+"~"+strparts[5]); //Schrijf naar file
		   
		   }
	   }
	
   }
  
  
   p.close();
   pcat.close();
  } catch (Exception e) {
   e.printStackTrace();
  }
 }
}

